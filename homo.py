# -*- coding: utf-8 -*-
"""
Created on Sun Jan 17 06:00:54 2021

@author: User
"""
import cv2
import os
import numpy as np
import argparse

parser = argparse.ArgumentParser(description = 'Get homography from court')
parser.add_argument('--input_video', type = str, help = 'input video name')
args = parser.parse_args()

class Hfinder(object):
    def __init__(self, img, court2D=[]):
        super(Hfinder, self).__init__()
        self.img = cv2.copyMakeBorder(img, 50,50,50,50, cv2.BORDER_CONSTANT, value=[0,0,0]) # padding
        self.court3D = [[0,0], [610,0], [610,1340], [0,1340]]
        self.court2D = court2D
        self.H = np.zeros((3,3))
        self.calculateH(self.img)
    def getH(self):
        return self.H
    def mouseEvent(self, event, x, y, flags, param):
        if event == cv2.EVENT_LBUTTONUP:
            if len(self.court2D) < 4:
                self.court2D.append([x, y])
            else:
                idx = np.linalg.norm(np.array(self.court2D) - np.array([[x, y]]), axis=1).argmin()
                self.court2D[idx] = [x, y]

    def calculateH(self, img):
        if len(self.court2D) == 0:
            cv2.namedWindow("Please pick 4 point of court")
            cv2.setMouseCallback("Please pick 4 point of court", self.mouseEvent)
            while True:
                show = np.copy(img)
                for c in self.court2D:
                    cv2.circle(show, (c[0], c[1]), 3, (38, 28, 235), -1)
                if len(self.court2D) > 1:
                    cv2.drawContours(show, [np.array(self.court2D)], 0, (38, 28, 235), 1)
                cv2.imshow("Please pick 4 point of court", show)
                key = cv2.waitKey(1)
                if key == 27:
                    cv2.destroyAllWindows()
                    break

        self.court2D = np.array(self.court2D) - np.array([[50,50]]) # unpadding
        print('up_left = (', self.court2D[0][0] ,',', self.court2D[0][1] ,')')
        print('up_right = (', self.court2D[1][0] ,',', self.court2D[1][1], ')')
        print('low_right = (', self.court2D[2][0] ,',', self.court2D[2][1], ')')
        print('low_left = (', self.court2D[3][0] ,',', self.court2D[3][1], ')')
        self.court3D = np.array(self.court3D)
        self.H, status = cv2.findHomography(self.court2D, self.court3D)

def getImage():

    cap = cv2.VideoCapture(args.input_video)
    # cap = cv2.VideoCapture("labeltool/2_20_15.mp4")
    success,image = cap.read()
    cap.release()
    count = 0
    if success:
        print('finish getting image process...')
        return image
    else:
        os._exit(0)
    

image = getImage()    
hf = Hfinder(image)
H = hf.getH()
print("Homography matrix : ")
print(H)

#s = cv2.FileStorage("mtx/Hmtx.yml", cv2.FileStorage_WRITE)
#s.write('Hmtx', H)
#s.release()