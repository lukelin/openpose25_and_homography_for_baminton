import json
import csv
import os
import cv2
import argparse
import numpy as np

up_left = ( 400 , 306 )
up_right = ( 842 , 310 )
low_right = ( 1058 , 712 )
low_left = ( 234 , 712 )

M = [[ 3.74440126e+00,  1.53096209e+00, -1.96623490e+03],
[-1.53325067e-01,  1.69424199e+01, -5.12305046e+03],
[-1.14421692e-04,  5.86947898e-03,  1.00000000e+00]]
M = np.array(M)

parser = argparse.ArgumentParser(description = 'Get skeleton from player')
parser.add_argument('--input_video', type = str, help = 'input video name')
args = parser.parse_args()

command = "./build/examples/openpose/openpose.bin --video {} --display 0 --write_json output_jsons/ --render_pose 0".format(args.input_video)
os.system(command)
video = cv2.VideoCapture(args.input_video)
total_frame=video.get(cv2.CAP_PROP_FRAME_COUNT)

def in_area(x, y):
    if y >= up_left[1] and y <= low_left[1]:
        slope = float(up_left[1]-low_left[1])/float(up_left[0]-low_left[0])
        offset = up_left[1] - up_left[0]*slope
        y_bound = x*slope + offset
        if y < y_bound:
            return False
        else:
            slope = float(up_right[1]-low_right[1])/float(up_right[0]-low_right[0])
            offset = up_right[1] - up_right[0]*slope
            y_bound = x*slope + offset
            if y < y_bound:
                return False
            else:
                return True
    else:
        return False

player_top = []
player_bot = []
miss = [-1]*75


for frame in range(int(total_frame)):
    with open('output_jsons/' + args.input_video[:-4] + '_' + str(frame).zfill(12) +'_keypoints.json') as f:
        data = json.load(f)
        
    player_top.append(miss)
    player_bot.append(miss)
    for i in range(len(data["people"])):
        if in_area(data["people"][i]["pose_keypoints_2d"][63], data["people"][i]["pose_keypoints_2d"][64]) or in_area(data["people"][i]["pose_keypoints_2d"][72], data["people"][i]["pose_keypoints_2d"][73]):
            x_i = data["people"][i]["pose_keypoints_2d"][63]
            y_i = data["people"][i]["pose_keypoints_2d"][64]
            p_c = (M @ np.array([[x_i], [y_i], [1]])).reshape(-1)
            x_c, y_c = (int(p_c[0]/p_c[2]), int(p_c[1]/p_c[2]))
            if(y_c >= 670):
                player_bot[frame] = data["people"][i]["pose_keypoints_2d"]
            else:
                player_top[frame] = data["people"][i]["pose_keypoints_2d"]
            
            #break
            
with open('top_player.csv','w', newline='') as csvfile1, open('bot_player.csv','w', newline='') as csvfile2:
    top = csv.writer(csvfile1)
    bot = csv.writer(csvfile2)
    top.writerow(['frame','x0', 'y0','c0','x1', 'y1','c1', 'x2', 'y2','c2', 'x3', 'y3','c3', 'x4', 'y4','c4', 'x5', 'y5','c5', 'x6', 'y6','c6','x7', 'y7','c7', 'x8', 'y8','c8', 'x9', 'y9','c9', 'x10', 'y10','c10', 'x11', 'y11','c11', 'x12', 'y12','c12', 'x13', 'y13','c13', 'x14','y14','c14', 'x15', 'y15','c15', 'x16', 'y16','c16', 'x17', 'y17','c17', 'x18', 'y18','c18', 'x19', 'y19','c19', 'x20', 'y20','c20', 'x21', 'y21','c21', 'x22', 'y22','c22', 'x23', 'y23','c23', 'x24', 'y24','c24'])
    bot.writerow(['frame','x0', 'y0','c0','x1', 'y1','c1', 'x2', 'y2','c2', 'x3', 'y3','c3', 'x4', 'y4','c4', 'x5', 'y5','c5', 'x6', 'y6','c6','x7', 'y7','c7', 'x8', 'y8','c8', 'x9', 'y9','c9', 'x10', 'y10','c10', 'x11', 'y11','c11', 'x12', 'y12','c12', 'x13', 'y13','c13', 'x14','y14','c14', 'x15', 'y15','c15', 'x16', 'y16','c16', 'x17', 'y17','c17', 'x18', 'y18','c18', 'x19', 'y19','c19', 'x20', 'y20','c20', 'x21', 'y21','c21', 'x22', 'y22','c22', 'x23', 'y23','c23', 'x24', 'y24','c24'])
    for i in range(len(player_bot)):
        bot.writerow([i] + player_bot[i])
    for i in range(len(player_top)):
        top.writerow([i] + player_top[i])

