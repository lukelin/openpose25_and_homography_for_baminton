###### tags: `CoachAI` `Openpose25` `GitLab`

# Openpose25 and Homography (GitLab)

## :gear: 1. Install

### Package

**Please refer to [Openpose github](https://github.com/CMU-Perceptual-Computing-Lab/openpose) for environment install and quick start**

- GitLab: https://gitlab.com/lukelin/openpose25_and_homography_for_baminton
- Docker file : docker pull cwaffles/openpose
        
        
        $ cd openpose
        $ git clone https://gitlab.com/lukelin/openpose25_and_homography_for_baminton.git


<br>
      
## :hammer_and_wrench: 2. Get homography from input video

`python3 homo.py --input_video=<input_video_path>`
    
Just put the video path you want to get matrix on option `<input_video_path>` . 

**Click left-top/right-top/right-down/left-down outer corner of the court in order. And you can adjust corner points after finishing click 4 corners.**

press `ESC` to get the result.

#### The result should look like : 

![](https://i.imgur.com/yn6z3ZU.png)

![](https://i.imgur.com/UAvK6HT.png)



<br>

## :runner: 3. Get openpose & divide top and bot player

### Step 1 : Copy the result from previous step
copy the coordinate of up_left / up_right / low_right / low_left and homography matrix, and paste on line 8-15 in `get_player_skeleton.py `

![](https://i.imgur.com/tWbJnY7.png)

### Step 2 : Get skeleton and divide 

`python3 get_player_skeleton.py --input_video=<input_video_path>`
    
Just put the video path you want to predict skeleton on option `<input_video_path>` . 

The code will filter out the skeleton out of court, and using left heel coordinate after homography mapping to divide top & bot player.


If y >= 670: <br>
    skeleton --> bot <br>
 Else: <br>
    skeleton --> top <br>


This is because the size of badminton court is 610*1340. <br>
![](https://i.imgur.com/fXwycZ5.jpg)


#### You will get `top_player.csv` and `bot_player.csv`
- In csv file <br>
 ![](https://i.imgur.com/CurbDDr.png)
```
  - frame represent the frame of video. 
  - x, y is the coordinate of keypoint in openpose25. x, y is 0 while openpose missing.
  - c is confidence score of keypoint  
```


## :art: 4. Draw skeleton of single player

`python3 show_skeleton.py --input_video=<input_video_path> --input_csv=<input_csv_path> --output_video=<output_video_path>`

`<input_video_path>` & `<input_csv_path>` are the video path & the csv path you want to draw, and `<output_video_path>` is the output video name. 

![](https://i.imgur.com/U3SXWMx.png)

**After command, the result should look like :**
![](https://i.imgur.com/HtgKUAo.gif)




























