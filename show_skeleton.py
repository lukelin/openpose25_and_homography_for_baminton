# -*- coding: utf-8 -*-
"""
Created on Sun Jan 17 04:57:40 2021

@author: User
"""
import csv
import cv2
import argparse

parser = argparse.ArgumentParser(description = 'Show skeleton of single player')
parser.add_argument('--input_video', type = str, help = 'input video name')
parser.add_argument('--input_csv', type = str, help = 'input csv name')
parser.add_argument('--output_video', type = str, default = 'output_skeleton.mp4', help = 'output video name')
args = parser.parse_args()

input_video_name = args.input_video
csv_name = args.input_csv
output_video_name = args.output_video

cap = cv2.VideoCapture(input_video_name)
fps = int(cap.get(cv2.CAP_PROP_FPS))
total_frame=cap.get(cv2.CAP_PROP_FRAME_COUNT)
fourcc = cv2.VideoWriter_fourcc(*'XVID')
output_width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
output_height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
out = cv2.VideoWriter(output_video_name, fourcc, fps, (output_width, output_height))

def draw_skeleton(img,points):
    POSE_PAIRS = [[0, 1], [1, 2], [2, 3], [3, 4], [1, 5], [5, 6], [6, 7], [1, 8], [8,9],[9,10],[10,11],[11,24],[11,22],[22,23],[8,12],[12,13],[13,14],[14,21],[14,19],[19,20],[0,15],[15,17],[0,16],[16,18]]
    for pair in POSE_PAIRS:
        partA = pair[0]
        partB = pair[1]

        if points[partA] and points[partB]:
            if(not (points[partA] == (-1, -1) or points[partA] == (0, 0))):
                cv2.circle(img, points[partA], 3, (0, 0, 255), thickness=-1, lineType=cv2.FILLED)
                
            if(not ((points[partA] == (-1, -1) or points[partA] == (0, 0)) or (points[partB] == (-1, -1) or points[partB] == (0, 0)))): 
                cv2.line(img, points[partA], points[partB], (0, 255, 255), 2)

points = []
with open(csv_name, newline='') as csvfile:

    rows = csv.DictReader(csvfile)
    for row in rows:
        points.append([(int(float(row['x0'])),int(float(row['y0']))),  (int(float(row['x1'])),int(float(row['y1']))), (int(float(row['x2'])),int(float(row['y2']))), (int(float(row['x3'])),int(float(row['y3']))), (int(float(row['x4'])),int(float(row['y4']))), (int(float(row['x5'])),int(float(row['y5']))), (int(float(row['x6'])),int(float(row['y6']))), (int(float(row['x7'])),int(float(row['y7']))), (int(float(row['x8'])),int(float(row['y8']))), (int(float(row['x9'])),int(float(row['y9']))), (int(float(row['x10'])),int(float(row['y10']))), (int(float(row['x11'])),int(float(row['y11']))), (int(float(row['x12'])),int(float(row['y12']))), (int(float(row['x13'])),int(float(row['y13']))), (int(float(row['x14'])),int(float(row['y14']))), (int(float(row['x15'])),int(float(row['y15']))), (int(float(row['x16'])),int(float(row['y16']))), (int(float(row['x17'])),int(float(row['y17']))), (int(float(row['x18'])),int(float(row['y18']))), (int(float(row['x19'])),int(float(row['y19']))), (int(float(row['x20'])),int(float(row['y20']))), (int(float(row['x21'])),int(float(row['y21']))), (int(float(row['x22'])),int(float(row['y22']))),(int(float(row['x23'])),int(float(row['y23']))), (int(float(row['x24'])),int(float(row['y24'])))])
count = 0

while(True):

  ret, frame = cap.read()
  if not ret:
      break
  draw_skeleton(frame, list(points[count]))
  #frame = cv2.circle(frame,(rightfootx[count], rightfooty[count]), 5, (255, 0, 0), -1)
  print("frame : ", count)
  out.write(frame)
  count+=1
  

cap.release()
out.release()

